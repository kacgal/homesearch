package dev.kacgal.homesearch.home.homeq

import com.fasterxml.jackson.annotation.JsonAlias
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.JsonNode
import dev.kacgal.homesearch.home.*
import java.time.LocalDate
import java.time.LocalDateTime

data class SearchListing(
    val type: String,
    val name: String?,
    val id: Int,
    val references: References,
    val location: Location,
    val address: Address,
    val shapes: List<String>,
    val videos: List<JsonNode>,
    @JsonProperty("indexed_at")
    val indexedAt: LocalDateTime,
    @JsonProperty("publish_date")
    val publishDate: LocalDate,
    @JsonProperty("access_date")
    val accessDate: ListOrSingle<LocalDate>?,
    val status: ListOrSingle<String>,
    val amenities: Map<String, ListOrSingle<Boolean>>,
    val audience: ListOrSingle<String>,
    @JsonProperty("short_lease")
    val shortLease: ListOrSingle<Boolean>?,
    @JsonProperty("has_ads")
    val hasAds: Boolean,
    @JsonProperty("sorting_mode")
    val sortingMode: ListOrSingle<String>?,
    val images: List<Image>,
    val rent: ListOrSingle<Int>,
    val area: ListOrSingle<Double>,
    val rooms: ListOrSingle<Double>,
    val floor: ListOrSingle<Int>,
    @JsonProperty("landlord_object_id")
    val landlordObjectId: String?,
    val standalone: Boolean?,
    @JsonProperty("floor_plan")
    val floorPlan: String?,
    val display: Display?,
)

data class Display(
    @JsonProperty("floor_plan")
    val floorPlan: String,
    @JsonProperty("landlord_object_id")
    val landlordObjectId: String,
    @JsonProperty("promotion_file")
    val promotionFile: String,
)

sealed class ListOrSingle<T> {
    class IsList<T>(val list: List<T>) : ListOrSingle<T>()
    class IsSingle<T>(val single: T) : ListOrSingle<T>()
}

data class Image(
    val image: String,
    val caption: String,
    val position: Int,
)

data class Address(
    val municipality: String,
    val county: String,
    val street: String?,
    @JsonProperty("street_number")
    val streetNumber: String?,
    val city: String?,
    val zip: String?,
)

data class Location(
    val lat: Double,
    val lon: Double,
)

data class References(
    val company: Int,
    val office: Int,
    val project: Int,
    val estate: Int?,
    val apartment: Int?,
    @JsonProperty("object_ad")
    val objectAd: Int?,
)

@JsonInclude(JsonInclude.Include.NON_NULL)
data class Search(
    val shapes: List<String>,
    val sorting: String,
    @JsonProperty("short_lease")
    val shortLease: Boolean,
    @JsonProperty("min_rent")
    val minRent: String? = null,
    @JsonProperty("max_rent")
    val maxRent: String? = null,
    @JsonProperty("min_room")
    val minRoom: String? = null,
    @JsonProperty("max_room")
    val maxRoom: String? = null,
    @JsonProperty("min_area")
    val minArea: String? = null,
    @JsonProperty("max_area")
    val maxArea: String? = null,
)

data class Suggestion(
    val text: String,
    val id: String,
    val hash: String,
    val path: String,
    val parent: SuggestionParent?,
)

data class SuggestionParent(
    val type: String,
    val id: String,
    val name: String,
)

data class ListResponse<E>(
    val results: List<E>,
    @JsonProperty("total_hits")
    @JsonAlias("total_suggestions")
    val total: Int
)

data class Query(
    val query: String,
)
