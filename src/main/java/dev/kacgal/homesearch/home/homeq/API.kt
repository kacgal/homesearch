package dev.kacgal.homesearch.home.homeq

import retrofit2.http.Body
import retrofit2.http.POST


interface HomeQGeoAPI {
    @POST("v1/public/suggest")
    suspend fun suggest(@Body query: Query): ListResponse<Suggestion>
}

interface HomeQSearchAPI {
    @POST("v2/search")
    suspend fun search(@Body search: Search): ListResponse<SearchListing>
}
