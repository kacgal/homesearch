package dev.kacgal.homesearch.home.homeq

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.JsonToken
import com.fasterxml.jackson.databind.BeanProperty
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JavaType
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.deser.ContextualDeserializer
import dev.kacgal.homesearch.Coordinates
import dev.kacgal.homesearch.SearchResult
import dev.kacgal.homesearch.home.HomeSearch
import okhttp3.OkHttpClient
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.create
import dev.kacgal.homesearch.Search as HSearch


class ListOrSingleDeserializer(
    private val type: JavaType? = null,
) : JsonDeserializer<ListOrSingle<*>>(), ContextualDeserializer {
    override fun deserialize(p: JsonParser, ctxt: DeserializationContext): ListOrSingle<*> {
        return when (p.currentToken()) {
            JsonToken.START_ARRAY -> {
                val type = ctxt.typeFactory.constructCollectionType(List::class.java, this.type)
                val value = ctxt.readValue<List<*>>(p, type)
                ListOrSingle.IsList(value)
            }
            else -> {
                val value = ctxt.readValue<Any>(p, this.type)
                ListOrSingle.IsSingle(value)
            }
        }
    }

    override fun createContextual(ctxt: DeserializationContext, property: BeanProperty): JsonDeserializer<*> {
        return ListOrSingleDeserializer(property.type.containedType(0))
    }
}

class HomeQSearch(client: OkHttpClient, factory: Converter.Factory) : HomeSearch {

    private val geoApi = Retrofit.Builder()
        .client(client)
        .baseUrl("https://geo.homeq.se/api/")
        .addConverterFactory(factory)
        .build()
        .create<HomeQGeoAPI>()
    private val searchApi = Retrofit.Builder()
        .client(client)
        .baseUrl("https://search.homeq.se/api/")
        .addConverterFactory(factory)
        .build()
        .create<HomeQSearchAPI>()

    override suspend fun search(search: HSearch): Iterator<SearchResult> {
        val area = this.geoApi.suggest(Query(search.query)).results.first()
        val result = this.searchApi.search(
            Search(
                shapes = listOf(area.id),
                sorting = "publish_date.desc",
                shortLease = false,
                minRent = search.price?.min?.toString(),
                maxRent = search.price?.max?.toString(),
                minRoom = search.rooms?.min?.toString(),
                maxRoom = search.rooms?.max?.toString(),
                minArea = search.area?.min?.toString(),
                maxArea = search.area?.max?.toString(),
            )
        )
        return result.results
            .map {
                val address = StringBuilder()
                it.address.street?.trimEnd(' ')?.let { street -> address.append(street) }
                it.address.streetNumber?.let { streetNumber -> address.append(' ').append(streetNumber) }
                it.address.zip?.let { zip -> address.append(", ").append(zip) }
                it.address.city?.trimEnd(' ')?.let { city ->
                    if (it.address.zip == null) {
                        address.append(',')
                    }
                    address.append(' ').append(city)
                }
                SearchResult(
                    id = it.id.toString(),
                    url = when (it.type) {
                        "project" -> "https://www.homeq.se/projekt/${it.id}"
                        "individual" -> "https://www.homeq.se/lagenhet/${it.id}"
                        else -> TODO("Unsupported result type: ${it.type}")
                    },
                    coordinates = Coordinates(it.location.lat, it.location.lon),
                    address = address.toString(),
                )
            }
            .iterator()
    }
}
