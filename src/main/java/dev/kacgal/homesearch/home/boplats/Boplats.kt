package dev.kacgal.homesearch.home.boplats

import dev.kacgal.homesearch.Coordinates
import dev.kacgal.homesearch.Search
import dev.kacgal.homesearch.SearchResult
import dev.kacgal.homesearch.home.HomeSearch
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import org.jsoup.Jsoup
import org.jsoup.nodes.Element
import org.jsoup.select.Evaluator

class OwnTextEqualsEvaluator(private val text: String) : Evaluator() {
    override fun matches(root: Element, element: Element): Boolean {
        return element.ownText().equals(this.text)
    }
}

class BoplatsSearch(private val client: OkHttpClient) : HomeSearch {
    override suspend fun search(search: Search): Iterator<SearchResult> {
        val area = search.x["x-boplats-area"] ?: search.query
        val areaId = getAreaId(area) ?: TODO("Region not found")

        val url = HttpUrl.Builder()
            .scheme("https")
            .host("nya.boplats.se")
            .addPathSegment("sok")
            .addQueryParameter("types", "1hand")
            .addQueryParameter("area", areaId)
        if (search.price?.max != null) {
            url.addQueryParameter("rent", search.price.max.toString())
        }
        if (search.area?.min != null) {
            url.addQueryParameter("squaremeters", search.area.min.toString())
        }
        if (search.rooms?.min != null) {
            url.addQueryParameter("rooms", search.rooms.min.toString())
        }

        val call = this.client.newCall(Request.Builder().url(url.build()).get().build())
        val response = call.execute()
        val body = response.body() ?: TODO("No response body?")
        val html = Jsoup.parse(body.string())
        val results = html.getElementById("search-result-items") ?: TODO("No results?")

        return results.children()
            .map {
                val searchResultLink = it.getElementsByClass("search-result-link").single()
                val link = searchResultLink.attr("href")
                val coordinates = getCoordinates(link)
                val address = searchResultLink.getElementsByClass("search-result-address").single().text()

                SearchResult(
                    id = link.split('/').last(),
                    url = link,
                    coordinates = coordinates,
                    address = address,
                )
            }
            .iterator()
    }

    private fun getCoordinates(url: String): Coordinates? {
        val call = this.client.newCall(Request.Builder().url(url).get().build())
        val response = call.execute()
        val body = response.body() ?: TODO("No response body?")
        val html = Jsoup.parse(body.string())
        val map = html.getElementById("karta") ?: return null
        return Coordinates(map.attr("data-latitude").toDouble(), map.attr("data-longitude").toDouble())
    }

    private fun getAreaId(area: String): String? {
        val call = this.client.newCall(Request.Builder().url("https://nya.boplats.se/filtrera").get().build())
        val response = call.execute()
        val body = response.body() ?: TODO("No response body?")
        val html = Jsoup.parse(body.string())
        return html.getElementById("select_city")?.selectFirst(OwnTextEqualsEvaluator(area))?.attr("value")
            ?: html.selectFirst(Evaluator.AttributeWithValue("data-city-title", area))?.attr("value")
    }
}
