package dev.kacgal.homesearch.home

import dev.kacgal.homesearch.Search
import dev.kacgal.homesearch.SearchResult

interface HomeSearch {
    suspend fun search(search: Search): Iterator<SearchResult>
}
