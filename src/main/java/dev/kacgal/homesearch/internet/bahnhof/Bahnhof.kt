package dev.kacgal.homesearch.internet.bahnhof

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import okhttp3.OkHttpClient
import okhttp3.Request
import org.jsoup.Jsoup
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.create
import retrofit2.http.*

class Bahnhof(private val client: OkHttpClient, factory: Converter.Factory) {

    private val api = Retrofit.Builder()
        .client(client)
        .baseUrl("https://bahnhof.se/ajax/")
        .addConverterFactory(factory)
        .build()
        .create<BahnhofAPI>()

    private fun getCSRF(): String {
        val call = this.client.newCall(Request.Builder().url("https://bahnhof.se/").get().build())
        val response = call.execute()
        val body = response.body() ?: TODO()
        val html = Jsoup.parse(body.string())
        return html.getElementById("csrf-token")?.attr("content") ?: TODO()
    }

    suspend fun getSpeeds(address: String): List<Int> {
        val csrf = this.getCSRF()
        return this.api.searchNetworks(csrf, Address(address))
            .data
            .networks
            ?.flatMap {
                this.api.getProducts(csrf, it.redirectUrl)
                    .data
                    .products
                    .filter { it.category.type == "BB" }
                    .map { it.title.split('/').first().toInt() }
            } ?: emptyList()
    }
}

interface BahnhofAPI {
    @POST("search/networks")
    @Headers(
        "X-Requested-With: XMLHttpRequest",
    )
    suspend fun searchNetworks(@Header("X-CSRF-TOKEN") csrf: String, @Body address: Address): Response<Networks>

    @GET("bredband/products/{id}")
    @Headers(
        "X-Requested-With: XMLHttpRequest",
    )
    suspend fun getProducts(
        @Header("X-CSRF-TOKEN") csrf: String,
        @Path("id", encoded = true) id: String
    ): Response<Products>
}

@JsonIgnoreProperties(ignoreUnknown = true)
data class Products(
    val products: List<Product>,
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class Product(
    val title: String,
    val price: Int,
    val category: Category,
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class Category(
    val type: String,
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class Response<T>(
    val status: String,
    val data: T
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class Networks(
    val type: String,
    val networks: List<Network>?,
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class Network(
    val redirectUrl: String,
)

data class Address(
    val address: String,
)
