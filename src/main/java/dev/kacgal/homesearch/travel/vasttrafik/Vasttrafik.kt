package dev.kacgal.homesearch.travel.vasttrafik

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import dev.kacgal.homesearch.Coordinates
import okhttp3.OkHttpClient
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.create
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query
import java.lang.reflect.Type
import java.time.Duration
import java.time.Instant
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

class QueryDateConverterFactory(format: DateTimeFormatter) : Converter.Factory() {

    private val converter = QueryDateConverter(format)

    override fun stringConverter(
        type: Type,
        annotations: Array<out Annotation>,
        retrofit: Retrofit
    ): Converter<out Any, String>? {
        return when (type) {
            ZonedDateTime::class.java -> this.converter
            else -> super.stringConverter(type, annotations, retrofit)
        }
    }
}

class QueryDateConverter(private val format: DateTimeFormatter) : Converter<ZonedDateTime, String> {
    override fun convert(value: ZonedDateTime): String = value.format(this.format)
}

class Vasttrafik(client: OkHttpClient, factory: Converter.Factory) {

    private val publicApi = Retrofit.Builder()
        .client(client)
        .baseUrl("https://www.vasttrafik.se/api/")
        .addConverterFactory(factory)
        .build()
        .create<PublicVasttrafikAPI>()

    private val api = Retrofit.Builder()
        .client(client)
        .baseUrl("https://api.vasttrafik.se/")
        .addConverterFactory(factory)
        .addConverterFactory(QueryDateConverterFactory(DateTimeFormatter.ISO_OFFSET_DATE_TIME))
        .build()
        .create<VasttrafikAPI>()

    private lateinit var token: String
    private var tokenExpires: Instant = Instant.MIN

    private suspend fun getAuth(): String {
        val now = Instant.now()
        if (now > tokenExpires) {
            val token = this.publicApi.newPublicToken()
            this.token = token.token
            this.tokenExpires = now + token.expiresIn
        }
        return "Bearer ${this.token}"
    }

    suspend fun getTravelTime(origin: Coordinates, destination: String, arrive: ZonedDateTime): Duration? {
        val auth = this.getAuth()
        val originName = this.api
            .getLocations(auth, "${origin.longitude}, ${origin.latitude}", listOf("address"), 1)
            .results
            .firstOrNull()
            ?.name ?: return null
        val destinationLocation = this.api
            .getLocations(auth, destination, listOf("stoparea", "metaStation", "address", "pointofinterest"), 1)
            .results
            .first()
        val lastJourney = this.api.searchJourney(
            auth,
            originName, origin.latitude, origin.longitude,
            destinationLocation.name, destinationLocation.latitude, destinationLocation.longitude,
            arrive,
            listOf("tram", "bus", "ferry", "taxi", "train", "walk"),
            listOf("vasttagen", "regionalTrain", "longDistanceTrain"),
            "1,0,2000", 0, 0, 0, "occupancy", true
        )
            .results
            .last { (it.arrivalAccessLink ?: it.tripLegs?.last())?.plannedArrivalTime?.isBefore(arrive) ?: false }
        val start = (lastJourney.departureAccessLink ?: lastJourney.tripLegs?.first())?.plannedDepartureTime
        val end = (lastJourney.arrivalAccessLink ?: lastJourney.tripLegs?.last())?.plannedArrivalTime
        return if (start == null || end == null){
            null
        } else {
            Duration.between(start, end)
        }
    }
}

@JsonIgnoreProperties(ignoreUnknown = true)
data class ListResult<T>(
    val results: List<T>,
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class Location(
    val name: String,
    val latitude: Double,
    val longitude: Double,
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class Journey(
    val departureAccessLink: DepartureArrival?,
    val tripLegs: List<DepartureArrival>?,
    val arrivalAccessLink: DepartureArrival?,
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class DepartureArrival(
    val plannedDepartureTime: ZonedDateTime,
    val plannedArrivalTime: ZonedDateTime,
)

interface VasttrafikAPI {

    @GET("pr/v3/locations/bytext")
    suspend fun getLocations(
        @Header("Authorization") auth: String,
        @Query("q") query: String,
        @Query("types") types: List<String>,
        @Query("limit") limit: Int,
    ): ListResult<Location>

    @GET("pr/v3/journeys?dateTimeRelatesTo=arrival")
    suspend fun searchJourney(
        @Header("Authorization") auth: String,
        @Query("originName") originName: String,
        @Query("originLatitude") originLatitude: Double,
        @Query("originLongitude") originLongitude: Double,
        @Query("destinationName") destinationName: String,
        @Query("destinationLatitude") destinationLatitude: Double,
        @Query("destinationLongitude") destinationLongitude: Double,
        @Query("dateTime") dateTime: ZonedDateTime,
        @Query("transportModes") transportModes: List<String>,
        @Query("transportSubModes") transportSubModes: List<String>,
        @Query("originWalk") originWalk: String,
        @Query("originCar") originCar: Int,
        @Query("originBike") originBike: Int,
        @Query("originPark") originPark: Int,
        @Query("includes") includes: String,
        @Query("includeNearbyStopAreas") includeNearbyStopAreas: Boolean,
    ): ListResult<Journey>
}

interface PublicVasttrafikAPI {
    @GET("token/public/new")
    suspend fun newPublicToken(): Token
}

data class Token(
    val token: String,
    val expiresIn: Duration,
)
