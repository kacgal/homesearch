package dev.kacgal.homesearch

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.module.SimpleModule
import com.fasterxml.jackson.dataformat.toml.TomlMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.addDeserializer
import com.fasterxml.jackson.module.kotlin.readValue
import dev.kacgal.homesearch.home.HomeSearch
import dev.kacgal.homesearch.home.boplats.BoplatsSearch
import dev.kacgal.homesearch.home.homeq.HomeQSearch
import dev.kacgal.homesearch.home.homeq.ListOrSingle
import dev.kacgal.homesearch.home.homeq.ListOrSingleDeserializer
import dev.kacgal.homesearch.internet.bahnhof.Bahnhof
import dev.kacgal.homesearch.travel.vasttrafik.Vasttrafik
import kotlinx.coroutines.runBlocking
import okhttp3.Cookie
import okhttp3.CookieJar
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.converter.jackson.JacksonConverterFactory
import java.io.File
import java.time.Duration
import java.time.ZonedDateTime
import java.time.temporal.TemporalAdjusters
import java.util.*


class MemoryCookieJar : CookieJar {

    private val cookies = mutableMapOf<String, MutableList<Cookie>>()

    override fun saveFromResponse(url: HttpUrl, cookies: List<Cookie>) {
        this.cookies.merge(url.host(), cookies.toMutableList()) { a, b -> (a + b).toMutableList() }
    }

    override fun loadForRequest(url: HttpUrl): List<Cookie> {
        val cookies = this.cookies.getOrPut(url.host()) { mutableListOf() }
        val now = System.currentTimeMillis()
        cookies.removeIf { it.expiresAt() < now }
        return cookies
    }

    fun empty() {
        this.cookies.clear()
    }
}


suspend fun main() {
    val retrofitJacksonFactory = JacksonConverterFactory.create(
        ObjectMapper().registerModules(
            KotlinModule.Builder().build(),
            JavaTimeModule(),
            SimpleModule()
                .addDeserializer(ListOrSingle::class, ListOrSingleDeserializer())
        )
    )

    val interceptor = HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.NONE }
    val cookies = MemoryCookieJar()
    val client = OkHttpClient.Builder()
        .cookieJar(cookies)
        .addInterceptor(interceptor)
        .build()

    val mapper = TomlMapper()
        .registerModules(
            KotlinModule.Builder().build(),
            JavaTimeModule(),
        )
    val config = mapper.readValue<Config>(File("config.toml"))

    val vasttrafik = Vasttrafik(client, retrofitJacksonFactory)
    val homeq = HomeQSearch(client, retrofitJacksonFactory)
    val boplats = BoplatsSearch(client)
    val bahnhof = Bahnhof(client, retrofitJacksonFactory)

    for (query in config.searches) {
        val results = MergeSearch(query, homeq, boplats)
        result@ for (result in results) {
            if (result.coordinates != null) {
                for (travel in query.travel) {
                    val max = Duration.ofMinutes(travel.maxTime.toLong())
                    val arrival = ZonedDateTime.now()
                        .with(TemporalAdjusters.next(travel.arrive.day))
                        .with(travel.arrive.time)
                    val duration = vasttrafik.getTravelTime(result.coordinates, travel.to, arrival)
                    if (duration == null) {
                        System.err.println("${result.url}: Travel to ${travel.to} unknown")
                    } else if (duration > max) {
                        System.err.println("${result.url}: Travel to ${travel.to} too long: ${duration.toMinutes()}")
                        continue@result
                    }
                }
            }
            if (query.minInternetSpeed != null && result.address.isNotEmpty()) {
                cookies.empty()
                val internetSpeeds = bahnhof.getSpeeds(result.address.replace("\\d{5} ".toRegex(), ""))
                if (internetSpeeds.none { it >= query.minInternetSpeed }) {
                    System.err.println("${result.url}: Internet too slow: $internetSpeeds")
                    continue@result
                }
            }
            println(result.url)
        }
    }
}

class MergeSearch(private val search: Search, vararg homes: HomeSearch) : Iterator<SearchResult> {

    private val homes = homes.iterator()
    private var current: Iterator<SearchResult> = Collections.emptyIterator()

    override fun hasNext(): Boolean {
        return this.homes.hasNext() || this.current.hasNext()
    }

    override fun next(): SearchResult {
        return if (current.hasNext()) {
            current.next()
        } else if (homes.hasNext()) {
            this.current = runBlocking { homes.next().search(search) }
            this.next()
        } else {
            throw RuntimeException("No more elements!")
        }
    }

}

data class Coordinates(
    val latitude: Double,
    val longitude: Double,
)

data class SearchResult(
    val id: String,
    val url: String,
    val coordinates: Coordinates?,
    val address: String,
)

