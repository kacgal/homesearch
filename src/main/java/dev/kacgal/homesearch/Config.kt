package dev.kacgal.homesearch

import com.fasterxml.jackson.annotation.JsonAnySetter
import com.fasterxml.jackson.annotation.JsonProperty
import java.time.DayOfWeek
import java.time.LocalTime

data class Config(
    @JsonProperty("search")
    val searches: List<Search>,
)

data class Search(
    val query: String,
    val price: MinMax<Int>?,
    val rooms: MinMax<Int>?,
    val area: MinMax<Int>?,
    @JsonProperty("min-internet-speed")
    val minInternetSpeed: Int?,
    val travel: List<Travel>,
    @JsonAnySetter
    val x: Map<String, String> = mutableMapOf(),
)

data class Travel(
    val to: String,
    @JsonProperty("max_time")
    val maxTime: Int,
    val arrive: Arrive,
)

data class Arrive(
    val time: LocalTime,
    val day: DayOfWeek,
)

data class MinMax<T>(
    val min: T?,
    val max: T?,
)
